const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');



const HtmlWebpackPluginSettings = new HtmlWebpackPlugin({
    title: 'Output Management'
});

const CleanWebpackPluginSettings = new CleanWebpackPlugin(['dist']);

module.exports = {
    entry: {
        index: './src/index.js',
    },
    plugins: [CleanWebpackPluginSettings, HtmlWebpackPluginSettings],
    output: {
        filename: '[name].bundle.js',
        chunkFilename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist')
    }, 
};